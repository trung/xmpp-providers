<!--
SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Maintaining the Provider List

This section should help contributors to maintain the [provider list](https://invent.kde.org/melvo/xmpp-providers/blob/master/providers.json).

The provider list is the main resource of this project.
Thus, it should be revised as often as possible and providers must be checked carefully.
We welcome everybody to contribute to this project and encourage you to ask questions in [Kaidan's support group chat](https://i.kaidan.im)!

Please [open an issue](https://invent.kde.org/melvo/xmpp-providers/-/issues) for each provider that should be added, updated or removed.
The issue's title should be of the form `providers: Add example.org`, `providers: Update example.org` or `providers: Remove example.org`.
If the properties of a provider in the list have changed, they can be updated.
If a provider in the list does not exist anymore, it can be removed.
If a project member creates a merge request to address your issue, you should reply to any questions.

**The following sections are only relevant to project members.**
If you are familiar with Git, GitLab and XMPP, you can become a project member too.
Please get in touch with us!

## Setup

Please follow the steps explained in [Kaidan's basic contribution setup](https://invent.kde.org/network/kaidan/-/wikis/setup) if you are unfamiliar with KDE Identity, GitLab or Git.
It should answer most of your questions.
You mostly have to replace `network/kaidan` by `melvo/xmpp-providers` in the text where necessary.

Set up everything needed to work inside of your local repository:
```
cd xmpp-providers
./setup.sh
```

## Rules

Please read the [README](/README.md) and stick to the following rules.

### For New Providers

* If a provider has **multiple domains** usable for XMPP, add only the domain that corresponds to the domain of the **provider's website**.
* Send a **test request** to each available support address and if you do not receive a **response within 7 days**, insert `"content": []` and `"comment": "admin@example.org: No response"` (`admin@example.org` must be replaced by the actual address).

### For All Providers

* Have a look at the **existing entries** in the provider list to understand the format and use one of them as a **template for new entries**.
* Add only [**properties**](https://invent.kde.org/melvo/xmpp-providers#properties) to the provider list that can be **verified** by statements on the provider's website.
* For all properties that can be verified by corresponding statements, provide a **reference** to the statement, depending on the property, in `source` or `content`.
* If there are **missing statements** for some properties, set the default value for `content`.
* Use the [filter script](https://invent.kde.org/melvo/xmpp-providers#filter-script) to see what criteria are not met yet for a specific category.
* The provider's server should be adjusted to meet the [criteria](https://invent.kde.org/melvo/xmpp-providers#criteria) for the best possible [category](https://invent.kde.org/melvo/xmpp-providers#categories).
* If you are one of the provider's **administrators**, adjust your server and add corresponding statements to your website.
* If you are an **uninvolved contributor**, ask the provider's administrators to do that.
* Once the adjustments are done, **references** to the statements can be added.
* Only include the **quantity** of units for measurement (e.g., `30` instead of `30 days`) in the provider list.
* **Sort** the entries in the provider list and the commit message in **alphabetically ascending** order by their JIDs.
* As soon as you made all changes to the provider list, save it, add all changes by `git add .` and commit them by `git commit`.
* A Git pre-commit hook is automatically run for identifying **syntax errors** and applying a consistent **format**.
* Use **different commits** for adding, updating and removing entries.
* If you add, update or remove entries, use the following **commit message**: `providers: Add / Update / Remove <added / updated / removed providers>` where `<added / updated / removed providers>` is replaced by the addresses of the providers or by `all providers`, each separated by a comma.
* Create a merge request for each commit or provide us the information via another channel (chat, email, online resource).

#### Commit Message Examples:

* `providers: Add example.com, example.org`
* `providers: Update example.com, example.net`
* `providers: Update all providers`
* `providers: Remove example.org`
