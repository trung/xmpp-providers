#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This script generates badges for specific categories.
"""
from __future__ import annotations

from typing import Any
from typing import cast

from argparse import ArgumentParser
import json
import logging
import os
import shutil

from common import Category
from common import create_parent_directories
from common import delete_parent_directory
from common import PROVIDERS_FILE_PATH

BADGE_TEMPLATE_FILE_PATH = "badge.svg"
BADGE_COUNT_TEMPLATE_FILE_PATH = "badge-count.svg"

PROVIDER_BADGE_FILES_PATH = "badges/%s.svg"

CATEGORY_PLACEHOLDER = "placeholder-category"
COLOR_PLACEHOLDER = "placeholder-color"
COUNT_PLACEHOLDER = "placeholder-count"

CATEGORIES_AND_COLORS = {
	Category.AUTOMATICALLY_CHOSEN: "439639",
	Category.MANUALLY_SELECTABLE: "a0ce67",
	Category.COMPLETELY_CUSTOMIZABLE: "e96d1f",
	Category.AUTOCOMPLETE: "d9101e",
}


class ApplicationArgumentParser(ArgumentParser):
	"""This is a parser for the arguments provided to run the application."""

	def __init__(self) -> None:
		super().__init__()

		self.description = "Generates badges for specific categories based on the provider lists which must be created beforehand."
		self.usage = "%(prog)s [-h] | [-q | -d] [-l] [-c] [-A | -B | -C | -D]"

		self.add_argument(
			"-q",
			"--quiet",
			help="log only errors",
			action="store_const",
			dest="log_level",
			const=logging.ERROR,
			default=logging.INFO,
		)

		self.add_argument(
			"-d",
			"--debug",
			help="log debug output",
			action="store_const",
			dest="log_level",
			const=logging.DEBUG,
			default=logging.INFO,
		)

		self.add_argument(
			"-l",
			"--links",
			help="create symbolic links to badges instead of creating regular files",
			action="store_true",
			dest="links",
		)

		self.add_argument(
			"-c",
			"--count",
			help="generate badges with count of providers in specific categories in addition to the normal badges",
			action="store_true",
			dest="count",
		)

		self.add_argument(
			"-A",
			"--category-A",
			help="generate only badge for category A",
			action="store_const",
			dest="category",
			const=Category.AUTOMATICALLY_CHOSEN,
			default=Category.ALL,
		)

		self.add_argument(
			"-B",
			"--category-B",
			help="generate only badge for category B",
			action="store_const",
			dest="category",
			const=Category.MANUALLY_SELECTABLE,
			default=Category.ALL,
		)

		self.add_argument(
			"-C",
			"--category-C",
			help="generate only badge for category C",
			action="store_const",
			dest="category",
			const=Category.COMPLETELY_CUSTOMIZABLE,
			default=Category.ALL,
		)

		self.add_argument(
			"-D",
			"--category-D",
			help="generate only badge for category D",
			action="store_const",
			dest="category",
			const=Category.AUTOCOMPLETE,
			default=Category.ALL,
		)


if __name__ == "__main__":
	arguments = ApplicationArgumentParser().parse_args()
	logging.basicConfig(level=arguments.log_level, format="%(levelname)s %(message)s")

	provider_badge_files_directory_path = os.path.dirname(PROVIDER_BADGE_FILES_PATH)
	if os.path.isdir(provider_badge_files_directory_path):
		logging.debug("Deleting parent directory '%s' for provider badge files", provider_badge_files_directory_path)
		delete_parent_directory(PROVIDER_BADGE_FILES_PATH)

	logging.debug("Creating parent directories '%s' for provider badge files", provider_badge_files_directory_path)
	create_parent_directories(PROVIDER_BADGE_FILES_PATH)

	badge_template_file_paths = [BADGE_TEMPLATE_FILE_PATH]
	if arguments.count:
		badge_template_file_paths.append(BADGE_COUNT_TEMPLATE_FILE_PATH)

	for badge_template_file_path in badge_template_file_paths:
		with open(badge_template_file_path, "r") as badge_template_file:
			logging.debug("Using template file '%s'", badge_template_file_path)

			is_badge_with_count_being_generated = badge_template_file_path == BADGE_COUNT_TEMPLATE_FILE_PATH
			badge_template_file_content = badge_template_file.read()

			category = cast(Category, arguments.category)
			categories = [category]

			if category == Category.ALL:
				categories = [
					Category.AUTOMATICALLY_CHOSEN,
					Category.MANUALLY_SELECTABLE,
					Category.COMPLETELY_CUSTOMIZABLE,
					Category.AUTOCOMPLETE,
				]

			categories_and_colors: dict[Category, str] = {}

			for category in categories:
				categories_and_colors[category] = CATEGORIES_AND_COLORS[category]

			for category, color in categories_and_colors.items():
				replacements = {
					CATEGORY_PLACEHOLDER: category.value,
					COLOR_PLACEHOLDER: color,
				}

				providers_file_path_parts = os.path.splitext(PROVIDERS_FILE_PATH)
				providers_file_name = providers_file_path_parts[0]
				providers_file_extension = providers_file_path_parts[1]

				provider_list_file_path = f"{providers_file_name}-{category.value}{providers_file_extension}"
				with open(provider_list_file_path, "r") as provider_list_file:
					try:
						providers = cast(dict[str, Any], json.load(provider_list_file))

						if is_badge_with_count_being_generated:
							logging.debug("Determing count of providers in category %s by provider list file '%s'", category.value, provider_list_file_path)
							count = str(len(providers))
							replacements[COUNT_PLACEHOLDER] = count

						logging.debug("Creating badge for category %s with replacements %s", category.value, replacements)

						badge_template_file_path_parts = os.path.splitext(badge_template_file_path)
						badge_template_file_name = badge_template_file_path_parts[0]
						badge_template_file_extension = badge_template_file_path_parts[1]

						badge_file_path = f"{badge_template_file_name}-{category.value}{badge_template_file_extension}"
						with open(badge_file_path, "w") as badge_file:
							badge_file_content = badge_template_file_content

							for placeholder, replacement in replacements.items():
								badge_file_content = badge_file_content.replace(placeholder, replacement)

							badge_file.write(badge_file_content)
							logging.info("'%s' created", badge_file_path)

						if not is_badge_with_count_being_generated:
							for provider in providers:
								provider_badge_file_path = cast(str, PROVIDER_BADGE_FILES_PATH % provider["jid"])

								if os.path.exists(provider_badge_file_path):
									logging.debug("Skipping creation of provider badge for category %s because it already exists for better category", category.value)
								else:
									if arguments.links:
										relative_badge_file_path = os.path.relpath(badge_file_path, provider_badge_files_directory_path)
										logging.debug("Creating provider badge link '%s' to '%s' for category %s", provider_badge_file_path, relative_badge_file_path, category.value)
										os.symlink(relative_badge_file_path, provider_badge_file_path)
									else:
										logging.debug("Creating provider badge file '%s' by copying '%s' for category %s", provider_badge_file_path, badge_file_path, category.value)
										shutil.copyfile(badge_file_path, provider_badge_file_path)

									logging.info("'%s' created", provider_badge_file_path)
					except json.decoder.JSONDecodeError as e:
						logging.error("'%s' has invalid JSON syntax: %s in line %s at column %s", provider_list_file_path, e.msg, e.lineno, e.colno)
