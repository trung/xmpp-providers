#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This file includes parts needed by all scripts.
"""
from enum import Enum
import os
import shutil

README_FILE_PATH = "README.md"
CONTRIBUTING_FILE_PATH = "CONTRIBUTING.md"
PROVIDERS_FILE_PATH = "providers.json"
CLIENTS_FILE_PATH = "clients.json"

JSON_OUTPUT_INDENTATION = "\t"


class Category(Enum):
	"""This contains the provider categories."""

	ALL = "a"
	AUTOMATICALLY_CHOSEN = "A"
	MANUALLY_SELECTABLE = "B"
	COMPLETELY_CUSTOMIZABLE = "C"
	AUTOCOMPLETE = "D"


def create_parent_directories(file_path: str) -> None:
	"""Creates all parent directories of a file.

	Parameters
	----------
	file_path : str
		path of the file
	"""

	directory_path = os.path.dirname(file_path)

	if len(directory_path) != 0:
		os.makedirs(directory_path, exist_ok=True)


def delete_parent_directory(file_path: str) -> None:
	"""Deletes the parent directory of a file.

	Parameters
	----------
	file_path : str
		path of the file
	"""

	directory_path = os.path.dirname(file_path)

	if len(directory_path) != 0:
		shutil.rmtree(directory_path)
